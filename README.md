# laravel-taggables

This is a package based on spatie/tags.

New features are:

- Translations without json (another table)
- Dynamic binding
- Nestable tags

## installation

## Usage

### Trait

```php
<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use KDA\Taggable\Models\Traits\HasTag;

class MyModel extends Model
{

    use HasTag;
 
   

}

```

### Dynamic binding

You can register any model to be taggable without inheritance or any trait. This comes with some limitations especially with other plugins.

To do so, create a new *Provider* file in *app/Providers* or use

`php artisan make:provider TagsServiceProvider`

in the boot method call the `registerTaggableModel` with any Model class in arguments

```php
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use KDA\Laravel\Attachments\Models\Attachment;
use KDA\Taggable\Facades\Tags;

class TagsServiceProvider extends ServiceProvider
{
   
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Tags::registerTaggableModel(Attachment::class);
    }
}

```

This will add the tags relationship and the created/deleted model event. When interacting with these bound models you'll have to use the `Facade`


## Tagging models 

these examples are all made from facade. But if you're using the model trait you can as well call any method from the model without the first argument.




## contribute

see [Contrbiute.md](CONTRIBUTE.md)
