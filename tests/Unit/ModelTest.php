<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Taggable\Models\Tag;
use KDA\Tests\TestCase;

class ModelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_tag()
    {
        $t = Tag::factory()->create();
        $this->assertNotNull($t);
    }

    /** @test */
    public function can_create_tag_with_children()
    {
        $t = Tag::factory()->create();
        $t2 = Tag::factory()->create(['parent_id' => $t->getKey()]);
        $this->assertNotNull($t);
        $this->assertNotNull($t2);
        $this->assertNotNull($t2->parent);
        $this->assertEquals($t->id, $t2->parent->id);
        $this->assertEquals($t->fresh()->children->count(), 1);

        $t3 = Tag::factory()->create(['parent_id' => $t->getKey()]);
        $this->assertEquals($t->fresh()->children->count(), 2);
    }

    /** @test */
    public function children_always_keeps_parent_group()
    {
        $t = Tag::factory()->create(['group' => 'test']);
        $t2 = Tag::factory()->create(['parent_id' => $t->getKey()]);

        $this->assertEquals($t->fresh()->group, $t2->group);
    }
    /** @test */
    public function changing_parent_group_affect_children()
    {
        $t = Tag::factory()->create(['group' => 'test']);
        $t2 = Tag::factory()->create(['parent_id' => $t->getKey(), 'group' => 'logo']);
        $this->assertEquals($t->fresh()->group, $t2->group);

        $t->group = 'hello';
        $t->save();
        $this->assertEquals($t->fresh()->group, $t2->fresh()->group);
    }

    /** @test */
    public function children_always_keeps_parent_group_2()
    {
        $t = Tag::factory()->create(['group' => 'test']);
        $t2 = Tag::factory()->create(['parent_id' => $t->getKey(), 'group' => 'logo']);
        $this->assertEquals($t->fresh()->group, $t2->group);
    }



    /** @test */
    public function tag_has_default_translation()
    {
        $t = Tag::factory()->create(['group' => 'test']);
        $this->assertNotNull($t->translations);
        $this->assertNotNull($t->translation);
    }

    /** @test */
    public function can_add_translation()
    {
        $t = Tag::factory()->create(['group' => 'test']);
        $this->assertNotNull($t->translations);
        $this->assertNotNull($t->translation);
        $this->assertEquals($t->fresh()->translations->count(), 1);

        $t->addTranslation('fr', 'super tag');
        $this->assertEquals($t->fresh()->translations->count(), 2);
    }

    /** @test */
    public function find_or_create()
    {
        $t = Tag::findOrCreate('Hello world');
        $this->assertNotNull($t);
        $this->assertNotNull($t->group);
        $this->assertEquals($t->fresh()->translations->count(), 1);

    }

    /** @test */
    public function find_or_create_group()
    {
        $t = Tag::findOrCreate('Hello world', 'test');
        $this->assertNotNull($t);
        $this->assertEquals($t->fresh()->translations->count(), 1);

    }

    /** @test */
    public function find_or_create_locale()
    {
        $t = Tag::findOrCreate('Hello world', 'test','de');
        $this->assertNotNull($t);
        $this->assertTrue($t->fresh()->translations->count() >=1);
      //  dump($t->fresh()->translations);

    }


    /** @test */
    public function find_or_create_locale_twice()
    {
        $t2 = Tag::findOrCreate('Hello world', 'test','de');
        $t = Tag::findOrCreate('Hello world', 'test','de');
        $this->assertNotNull($t);
        $this->assertTrue($t->fresh()->translations->count() >=1);
        $this->assertEquals($t->id,$t2->id);

    }


    /** @test */
    public function find_from_string()
    {
        $t = Tag::findOrCreate('Hello world', 'test','de');
        $t2 = Tag::findFromString('Hello world','test','de');
        $this->assertEquals($t->id,$t2->id);
    }
}
