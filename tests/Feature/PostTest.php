<?php

namespace KDA\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Taggable\Models\Tag;
use KDA\Tests\Models\Post;
use KDA\Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

     /** @test */
     public function can_attachTags()
     {
        $post = Post::factory()->create();
        $post->attachTags(['Hello world','another']);
        
        $this->assertEquals($post->tags->count(),2);
     }

     /** @test */
     public function can_attachTag()
     {
        $post = Post::factory()->create();
        $post->attachTag('Hello world');
        $this->assertEquals($post->tags->count(),1);

     }
 
      /** @test */
      public function can_detachTags()
      {
         $post = Post::factory()->create();
         $post->attachTag('Hello world');
         $post->attachTags(['test','test2']);
         $this->assertEquals(3,$post->tags->count());


         $post->detachTags(['test','Hello world']);
         $this->assertEquals(1,$post->refresh()->tags->count());
 
      }


      /** @test */
      public function can_detachTag()
      {
         $post = Post::factory()->create();
         $post->attachTag('Hello world');
         $post->attachTags(['test','test2']);
         $this->assertEquals(3,$post->tags->count());


         $post->detachTag('test2');
         $this->assertEquals(2,$post->fresh()->tags->count());
 
      }


      /** @test */
      public function can_sync_tags()
      {
         $post = Post::factory()->create();
         $post->attachTag('Hello world');
         $post->attachTags(['test','test2']);
         $this->assertEquals(3,$post->tags->count());


         $post->syncTags(['test2']);
         $this->assertEquals(1,$post->fresh()->tags->count());
 
      }

}
