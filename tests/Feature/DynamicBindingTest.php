<?php

namespace KDA\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Taggable\Models\Tag;
use KDA\Taggable\Facades\Tags;
use KDA\Tests\Models\PostWithoutTrait;
use KDA\Tests\TestCase;

class DynamicBindingTest extends TestCase
{
    use RefreshDatabase;

     /** @test */
     public function can_attachTags()
     {
      Tags::registerTaggableModel(PostWithoutTrait::class);
        $post = PostWithoutTrait::factory()->create();
       // $post->attachTags(['Hello world','another']);
         Tags::attachTags($post,['Hello world','another']);
        $this->assertEquals(2,$post->refresh()->tags->count());
     }
 /** @test */
 public function can_detachTags()
 {
   Tags::registerTaggableModel(PostWithoutTrait::class);
    $post = PostWithoutTrait::factory()->create();
    Tags::attachTags($post,['Hello world','another']);

     Tags::detachTags($post,['Hello world']);
    $this->assertEquals(1,$post->refresh()->tags->count());
 }

     /** @test */
 public function modelDeleteEventisCalled()
 {
   Tags::registerTaggableModel(PostWithoutTrait::class);

    $post = PostWithoutTrait::factory()->create();
    Tags::attachTags($post,['Hello world','another']);
    $this->assertDatabaseCount('taggables', 2);

    $this->assertEquals(2,$post->refresh()->tags->count());

    $post->delete();
    
    $this->assertDatabaseCount('taggables', 0);
     
   
 }

}
