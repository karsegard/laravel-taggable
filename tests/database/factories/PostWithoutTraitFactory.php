<?php

namespace KDA\Tests\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Tests\Models\PostWithoutTrait;

class PostWithoutTraitFactory extends Factory
{
    protected $model = PostWithoutTrait::class;

    public function definition()
    {
        return [
            'title' => $this->faker->sentence(4),
        ];
    }
}
