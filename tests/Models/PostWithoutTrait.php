<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostWithoutTrait extends Model
{
    use HasFactory;

    public $table="posts";
    protected $fillable = [
        'title',
    ];


    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostWithoutTraitFactory::new();
    }
}
