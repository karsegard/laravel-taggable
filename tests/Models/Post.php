<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Taggable\Models\Traits\HasTag;

class Post extends Model
{
    use HasFactory;
    use HasTag;

    protected $fillable = [
        'title',
    ];


    protected static function newFactory()
    {
        return  \KDA\Tests\Database\Factories\PostFactory::new();
    }
}
