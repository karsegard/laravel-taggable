<?php

namespace KDA\Taggable\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Taggable\Models\Tag;

class TagTranslationFactory extends Factory
{
    protected $model = Tag::class;

    public function definition()
    {
        return [
            'name' => $this->faker->word(),
            'locale' => $this->faker->locale(),
            'tag_id' => Tag::factory(),
        ];
    }
}
