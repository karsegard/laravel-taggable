<?php

namespace KDA\Taggable\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Taggable\Models\Tag;

class TagFactory extends Factory
{
    protected $model = Tag::class;

    public function definition()
    {
        return [
            'name' => $this->faker->words(13, true),
        ];
    }
}
