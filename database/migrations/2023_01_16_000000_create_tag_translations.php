<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Taggable\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create(ServiceProvider::getTableName('tag_translations'), function (Blueprint $table) {
            $table->id();
            $table->foreignId('tag_id')->constrained(ServiceProvider::getTableName('tags'))->onDelete('cascade');
            $table->string('name');
            $table->string('locale');
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(ServiceProvider::getTableName('tag_translations'));
        Schema::enableForeignKeyConstraints();
    }
};
