<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Taggable\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create(ServiceProvider::getTableName('taggables'), function (Blueprint $table) {
            $table->foreignId('tag_id')
                ->references('id')
                ->on('tags')
                ->onDelete('cascade');
            $table->numericMorphs('taggable');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(ServiceProvider::getTableName('taggables'));
        Schema::enableForeignKeyConstraints();
    }
};
