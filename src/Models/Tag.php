<?php

namespace KDA\Taggable\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use KDA\Eloquent\DefaultAttributes\Models\Traits\HasDefaultAttributes;
use KDA\Taggable\Database\Factories\TagFactory;
use KDA\Taggable\Facades\Tags;
use KDA\Taggable\ServiceProvider;

class Tag extends Model
{
    use HasFactory;
    use HasDefaultAttributes;

    protected $fillable = [
        'name',
        'group',
        'parent_id',
        'meta'
    ];

    protected $casts = [
        'meta'=>'json'
    ];
    protected $appends = [
        'translation',
    ];

    public function getTable()
    {
        return ServiceProvider::getTableName('tags');
    }

    protected function applyDefaultAttributes()
    {
        if ($this->group != $this->getOriginal('group')) {
           $this->children->each(function($t){
                $t->group = $this->group;
                $t->save();
           });
        }
    }

    protected function updatedDefaultAttributes(){
   //     $this->setParentGroup();
    }

    public function scopeForGroup($q,$group){
        return $q->where('group',$group);
    }

    public function scopeWithoutParent($q){
        return $q->whereNull('parent_id');
    }
    
    public static function create(array $attributes = [],$locale =null)
    {
        $locale = $locale ??  $data['locale'] ?? static::getLocale();

        $name = $attributes['name'];
        $slug = \Str::slug($name);
     //   $attributes['name']=$slug;
        $model = static::query()->create($attributes);
        
        if($locale !== static::getLocale()){
           TagTranslation::create(['name' => $name, 'locale' => $locale, 'tag_id' => $model->id]);
        }
        Tag::withoutEvents(function() use ($model,$slug){
            $model->name=$slug;
            $model->save();
        });
        
        return $model;
    }

    /*public function setNameAttribute($value){
        $value = \Str::slug($value);
        $this->attributes['name']=$value;
    }*/
    protected function createdDefaultAttributes()
    {
     //   $this->setParentGroup();
       $locale =  static::getLocale();
        if($this->refresh()->translations->count()==0){
            TagTranslation::create(['name' => $this->name, 'locale' => $locale, 'tag_id' => $this->id]);

        }
    }


   /* protected function setParentGroup(){
        if (! blank($this->parent)) {
            $this->group = $this->parent->fresh()->group;
        }
    }*/

    protected static function newFactory()
    {
        return  TagFactory::new();
    }

    public function children()
    {
        return $this->hasMany(Tag::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(Tag::class, 'parent_id');
    }

    public function translations()
    {
        return $this->hasMany(TagTranslation::class, 'tag_id');
    }

    public function getTranslationAttribute()
    {
       /* dump($this->translations()->where('locale', static::getLocale())
        ->orWhere(function($q){
            $q->where('tag_id',$this->id)->whereNotNull('locale');
        })->get());

        return $this->translations()->where('locale', static::getLocale())
        ->orWhere(function($q){
            $q->where('tag_id',$this->id)->whereNotNull('locale');
        })?->first();*/

        $tr = $this->translations->where('locale',static::getLocale())->first();
        if(!$tr){
            return $this->translations?->first()?->name;
        }
        return $tr->name;
    }

    public function getTranslationWithKeyAttribute()
    {
       /* dump($this->translations()->where('locale', static::getLocale())
        ->orWhere(function($q){
            $q->where('tag_id',$this->id)->whereNotNull('locale');
        })->get());

        return $this->translations()->where('locale', static::getLocale())
        ->orWhere(function($q){
            $q->where('tag_id',$this->id)->whereNotNull('locale');
        })?->first();*/

        $tr = $this->translations->where('locale',static::getLocale())->first();
        if(!$tr){
            return $this->translations->first()->name;
        }
        return $this->name.':'.$tr->name;
    }

    public function getGroupAttribute(){
        return $this->parent ? $this->parent->group : $this->attributes['group'] ?? '';
    }

    public function addTranslation($locale,$name)
    {
        Tags::addTranslation($this,$locale,$name);
    }


    public static function getLocale()
    {
        return app()->getLocale();
    }

    public static function findOrCreate(
        string | array | ArrayAccess $values,
        string | null $group = '',
        string | null $locale = null,
    ): Collection | Tag | static {
        $tags = collect($values)->map(function ($value) use ($group, $locale) {
            if ($value instanceof self) {
                return $value;
            }

            return static::findOrCreateFromString($value, $group, $locale);
        });

        return is_string($values) ? $tags->first() : $tags;
    }

    protected static function findOrCreateFromString(string $name, string $group = '', string $locale = null)
    {
        $locale = $locale ?? static::getLocale();

        $tag = static::findFromString($name, $group, $locale);

        if (! $tag) {
            $tag = static::create([
                'name' => $name,
                'group' => $group
               
            ],$locale);
        }

        return $tag;
    }

    public static function findFromString(string $name, string $group = null, string $locale = null)
    {
        $locale = $locale ?? static::getLocale();
       // $name = \Str::slug($name);
        return static::query()
            ->whereHas('translations',function($q) use($name,$locale){
                $q->where('name',$name)->where('locale',$locale);
            })
            ->where('group', $group)
            ->first();
    }
    public function getAncestorsAttribute()
    {
        $result = [];

        $parent = $this->parent;
        while ($parent) {
            $result[] = $parent;
            $parent = $parent->parent;
        }

        return array_reverse($result);
    }

    public static function exists($id,$group): Tag | bool
    {
        $tag = Tag::forGroup($group)->find($id);
        return !blank($tag) ? $tag : false;
    }
}
