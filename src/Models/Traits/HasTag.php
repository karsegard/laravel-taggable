<?php

namespace KDA\Taggable\Models\Traits;

use ArrayAccess;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use InvalidArgumentException;
use KDA\Taggable\Facades\Tags;
use KDA\Taggable\Models\Tag;
/*
This is adapted from spatie/tags 
to keep basic compatibility
*/

trait HasTag
{
    public array $queuedTags = [];

    public static function getTagClassName(): string
    {
        return Tag::class;
    }



    public static function bootHasTag()
    {
        static::created(Tags::getModelCreatedClosure());

        static::deleted(Tags::getModelDeletedClosure());
    }

    public function attachTags(array | ArrayAccess | Tag $tags, string $group = ''): static
    {
        /* $className = static::getTagClassName();

        $tags = collect($className::findOrCreate($tags, $group));

        $this->tags()->syncWithoutDetaching($tags->pluck('id')->toArray());

        return $this;*/

        Tags::attachTags($this, $tags, $group);
        return $this;
    }

    public function attachTag(string | Tag $tag, string | null $group = '')
    {
        return $this->attachTags([$tag], $group);
    }

    protected static function convertToTags($values, $group = '', $locale = null)
    {
        if ($values instanceof Tag) {
            $values = [$values];
        }

        return collect($values)->map(function ($value) use ($group, $locale) {
            if ($value instanceof Tag) {

                return $value;
            }
            $className = static::getTagClassName();

            if (is_numeric($value)) {
                return $className::find($value);
            }


            return $className::findFromString($value, $group, $locale);
        });
    }


    public function detachTags(array | ArrayAccess $tags, string | null $group = ''): static
    {
        Tags::detachTags($this, $tags, $group);
        return $this;
    }

    public function clearTags(string | null $group = ''): static
    {
        Tags::clearTags($this, $group);

        return $this;
    }

    public function detachTag(string | Tag $tag, string | null $group = ''): static
    {
        Tags::detachTag($this, $tag, $group);
        return $this;
    }

    public function setTagsAttribute(string | array | ArrayAccess | Tag $tags)
    {
        if (!$this->exists) {
            $this->queuedTags = $tags;
            return;
        }
        $this->syncTags($tags);
    }

    public function syncTags(string | array | ArrayAccess $tags): static
    {
        if (is_string($tags)) {
            $tags = Arr::wrap($tags);
        }

        $className = static::getTagClassName();

        $tags = collect($className::findOrCreate($tags));

        $this->tags()->sync($tags->pluck('id')->toArray());

        return $this;
    }

    public function syncTagsWithType(array | ArrayAccess $tags, string | null $group = null): static
    {
        $className = static::getTagClassName();

        $tags = collect($className::findOrCreate($tags, $group));

        $this->syncTagIds($tags->pluck('id')->toArray(), $group);

        return $this;
    }

    public function syncExistingTagsIdsWithType(array | ArrayAccess $tags, string | null $group = ''): static
    {
        $className = static::getTagClassName();

        $tags = collect($className::whereIn('id', $tags)->where('group', $group)->get());

        $this->syncTagIds($tags->pluck('id')->toArray(), $group);

        return $this;
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable',Tags::getPivotTableName());
    }

    

    protected function syncTagIds($ids, string | null $group = '', $detaching = true): void
    {
        $isUpdated = false;

        // Get a list of tag_ids for all current tags
        $current = $this->tags()
            ->newPivotStatement()
            ->where('taggable_id', $this->getKey())
            ->where('taggable_type', $this->getMorphClass())
            ->when($group !== null, function ($query) use ($group) {
                $tagModel = $this->tags()->getRelated();

                return $query->join(
                    $tagModel->getTable(),
                    'taggables.tag_id',
                    '=',
                    $tagModel->getTable() . '.' . $tagModel->getKeyName()
                )
                    ->where($tagModel->getTable() . '.group', $group);
            })
            ->pluck('tag_id')
            ->all();

        // Compare to the list of ids given to find the tags to remove
        $detach = array_diff($current, $ids);
        if ($detaching && count($detach) > 0) {
            $this->tags()->detach($detach);
            $isUpdated = true;
        }

        // Attach any new ids
        $attach = array_unique(array_diff($ids, $current));
        if (count($attach) > 0) {
            collect($attach)->each(function ($id) {
                $this->tags()->attach($id, []);
            });
            $isUpdated = true;
        }

        // Once we have finished attaching or detaching the records, we will see if we
        // have done any attaching or detaching, and if we have we will touch these
        // relationships if they are configured to touch on any database updates.
        if ($isUpdated) {
            $this->tags()->touchIfTouching();
        }
    }
    public function tagsWithType(string $group = ''): Collection
    {
        return Tags::tagsWithType($this, $group);
    }
    public function translatedTagsWithType(string $group = ''): Collection
    {
        return $this->tagsWithType($group)->map(function ($tag) {
            return $tag->translation;
        });
    }


    public function scopeWithTag($q, Tag | int $tag, $group = '')
    {
        if (is_numeric($tag)) {
            $tag = Tag::find($tag);
        }
        return $q->whereHas('tags', function ($q) use ($group, $tag) {
            return $q->where('tags.id', $tag->getKey())->where('group', $group);
        });
    }

    public function scopeWithAnyTag($q, array $tags, $group = '')
    {

        $tags = $this->convertToTags($tags, $group);
        $ids = $tags->map(fn ($tag) => $tag->getKey());

        return $q->whereHas('tags', function ($q) use ($group, $ids) {
            return $q->whereIn('tags.id', $ids)->where('group', $group);
        });
    }

    public function scopeWithAllTags($q, array $tags, $group = '')
    {

        $tags = $this->convertToTags($tags, $group);
       // $ids = $tags->map(fn ($tag) => $tag->getKey());

       
        collect($tags)->each(function ($tag) use ($q) {
            $q->whereHas('tags', function ( $q) use ($tag) {
                $q->where('tags.id', $tag->id ?? 0);
            });
        });
    }

    public function scopeWithAnyTranslatedTag($q, array $tags, $group,$locale=null)
    {
        $locale = $locale ?? Tag::getLocale();

        return $q->whereHas('tags', function ($q) use ($group, $locale,$tags) {
            // return $q->whereIn('tags.id',$ids)->where('group',$group);
            return  $q->whereHas('translations', function ($q) use ( $locale,$tags) {
                $q->where(function ($q) use ($tags){
                    foreach ($tags as $tag){
                        $q->orWhere('name', $tag)->orWhere('name', 'like','%'.$tag.'%');
                     }
                })->where('locale', $locale);
            })
            ->where('group', $group);
        });
    }

    public function scopeHavingTagsInGroup($q,$group){
        return $q->whereHas('tags',function($q) use ($group){
            $q->where('group',$group);
        });
    }
    public function scopeNotHavingTagsInGroup($q,$group){
        return $q->whereDoesntHave('tags',function($q) use($group){
            $q->where('group',$group);
        });
    }
}
