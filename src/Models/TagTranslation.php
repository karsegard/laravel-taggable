<?php

namespace KDA\Taggable\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Taggable\Database\Factories\TagTranslationFactory;
use KDA\Taggable\ServiceProvider;

class TagTranslation extends Model
{
    use HasFactory;

    protected $fillable = [
        'tag_id',
        'name',
        'locale',
    ];

    protected $casts = [

    ];

    public function getTable()
    {
        return ServiceProvider::getTableName('tag_translations');
    }
    
    protected static function newFactory()
    {
        return  TagTranslationFactory::new();
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'parent_id');
    }

    
}
