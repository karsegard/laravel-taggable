<?php

namespace KDA\Taggable\Library;

use Closure;
use Illuminate\Database\Eloquent\Model;
use KDA\Taggable\Models\Tag;
use KDA\Taggable\Models\TagTranslation;
use KDA\Laravel\Concerns\CanIgnoreMigrations;
use KDA\Taggable\Facades\Tags;
use Illuminate\Support\Collection;
use KDA\Taggable\ServiceProvider;

class TagManager
{
    use CanIgnoreMigrations;

    public static function getTagClassName(): string
    {
        return Tag::class;
    }

    public function getPivotTableName(){
        return ServiceProvider::getTableName('taggables');
    }

    
    public function addTranslation(Tag $tag, $locale, $name)
    {
        TagTranslation::create([
            'tag_id'=>$tag->getKey(),
            'locale'=>$locale,
            'name'=>$name
        ]);
    }

    public function isSoftDeleting($model):bool{
        return in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($model)) && ! $model->isForceDeleting();
    }

    public function forGroup(string $group){
        return Tag::forGroup($group)->get();
    }

    public function getModelCreatedClosure():Closure
    {
        return function (Model $taggableModel) {
            if (count($taggableModel->queuedTags??[]) === 0) {
                return;
            }

            $taggableModel->attachTags($taggableModel->queuedTags);

            $taggableModel->queuedTags = [];
        };
    }
    public function getModelDeletedClosure():Closure
    {
        
        return function (Model $deletedModel) {
            if(!Tags::isSoftDeleting($deletedModel)){
                Tags::clearAllTags($deletedModel);
            }
        };
    }

    public function registerTaggableModel($model,  $withHooks = true)
    {
        $model::resolveRelationUsing('tags', function ($model) {
            return $model->morphToMany(Tag::class, 'taggable');
        });

       
        if ($withHooks) {
            $model::created(Tags::getModelCreatedClosure());
            $model::deleted(Tags::getModelDeletedClosure());
        }
    }


    public function syncExistingTagsIdsWithType(Model $model,array | ArrayAccess $tags, string | null $group = ''): static
    {
        $className = static::getTagClassName();
      //  dump($tags);
      
        $tags = collect($className::whereIn('id',$tags)->where('group',$group)->get());
      //  dump($tags->pluck('id'));
        $this->syncTagIds($model,$tags->pluck('id')->toArray(), $group);

        return $this;
    }

    public function syncTagsIdsWithType(Model $model,array | ArrayAccess $tags, string | null $group = '',$detach=false): static
    {
     
        $this->syncTagIds($model,$tags, $group,$detach);

        return $this;
    }

    public function syncExistingTagsInGroup(Model $model, $tags, string $group=''):static
    {
        $className = static::getTagClassName();

        $tags = collect($tags)->reduce(function($carry,$tag) use ($className){
            if(is_subclass_of($tag,$className)){
                $carry[]=$tag->getKey();
            }else if(is_numeric($tag)){
                $carry[]=$tag;
            }
            return $carry;
        },[]);

        return $this->syncExistingTagsIdsWithType($model,$tags,$group);
    }

    protected function syncTagIds(Model $model, $ids, string | null $group = '', $detaching = true): void
    {
        $isUpdated = false;

        // Get a list of tag_ids for all current tags
        $current = $model->tags()
            ->newPivotStatement()
            ->where('taggable_id', $model->getKey())
            ->where('taggable_type', $model->getMorphClass())
            ->when($group !== null, function ($query) use ($group,$model) {
                $tagModel = $model->tags()->getRelated();

                return $query->join(
                    $tagModel->getTable(),
                    $this->getPivotTableName().'.tag_id',
                    '=',
                    $tagModel->getTable() . '.' . $tagModel->getKeyName()
                )
                    ->where($tagModel->getTable() . '.group', $group);
            })
            ->pluck('tag_id')
            ->all();

        // Compare to the list of ids given to find the tags to remove
        $detach = array_diff($current, $ids);
        if ($detaching && count($detach) > 0) {
            $model->tags()->detach($detach);
            $isUpdated = true;
        }

        // Attach any new ids
        $attach = array_unique(array_diff($ids, $current));
        if (count($attach) > 0) {
            collect($attach)->each(function ($id)  use ($model){
                $model->tags()->attach($id, []);
            });
            $isUpdated = true;
        }

        // Once we have finished attaching or detaching the records, we will see if we
        // have done any attaching or detaching, and if we have we will touch these
        // relationships if they are configured to touch on any database updates.
        if ($isUpdated) {
            $model->tags()->touchIfTouching();
        }
    }

    public function convertToTags($values, $group = '', $locale = null)
    {
        if ($values instanceof Tag) {
            $values = [$values];
        }

        return collect($values)->map(function ($value) use ($group, $locale) {
            if ($value instanceof Tag) {
             
                return $value;
            }
            $className = static::getTagClassName();

            if(is_numeric($value)){
                return $className::find($value);
            }


            return $className::findFromString($value, $group, $locale);
        });
    }

    public function detachTags(Model $model, array | ArrayAccess $tags, string | null $group = ''): static
    {
        $tags = Tags::convertToTags($tags, $group);
       
        collect($tags)
            ->filter()
            ->each(fn (Tag $tag) => $model->tags()->detach($tag));

        return $this;
    }
    public function clearAllTags(Model $model): static
    {
        $tags = $model->tags;
        collect($tags)
            ->filter()
            ->each(fn (Tag $tag) => $model->tags()->detach($tag));

        return $this;
    }
    public function clearTags(Model $model, string | null $group = ''): static
    {
        $tags = $model->tags->where('group',$group);
        collect($tags)
            ->filter()
            ->each(fn (Tag $tag) => $model->tags()->detach($tag));

        return $this;
    }

    public function detachTag(Model $model, string | Tag $tag, string | null $group = ''): static
    {
        return Tags::detachTags($model,[$tag], $group);
    }

    public function attachTags(Model $model, array | ArrayAccess | Tag $tags, string $group = ''): Model
    {
        $className = static::getTagClassName();

        $tags = collect($className::findOrCreate($tags, $group));

        $model->tags()->syncWithoutDetaching($tags->pluck('id')->toArray());

        return $model;
    }

    public function attachTag(Model $model,string | Tag $tag, string | null $group = '')
    {
        return $this->attachTags($model,[$tag], $group);
    }

    public function tagsWithType($model,string $group = ''): Collection
    {
        return $model->tags->filter(fn (Tag $tag) => $tag->group === $group);
    }
}
