<?php

namespace KDA\Taggable;

use Closure;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasConfigurableTableNames;
use KDA\Taggable\Facades\Tags as Facade;
use KDA\Taggable\Library\TagManager as Library;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasLoadableMigration;
    use \KDA\Laravel\Traits\HasMigration;
    use \KDA\Laravel\Traits\HasDumps;
    use HasConfigurableTableNames;
    use HasConfig;

    protected $packageName = 'kda-tags';
    protected $configs = [
        'kda/tags.php' => 'kda.tags',
    ];
    protected static $tables_config_key = 'kda.tags.tables';
    //  use \KDA\Laravel\Traits\ProvidesBlueprint;
    protected $dumps = [
        'tags',
        'taggables',
        'tag_translations'
    ];

    protected function shouldLoadMigration(){
        return Facade::shouldRunMigrations();
    }
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function register()
    {
        $this->app->singleton(Facade::class, function ($app) {
            return new Library();
        });
    }

  
}
